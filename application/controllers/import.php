<?php

class Import extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('login_manager');
    $this->load->library('pagination');
	}



  function index()
  {
    redirect('/projects/overview/', 'refresh');
  }


  function _parse($string)
  {
    $res =  preg_split ('/$\R?^/m',  $string);
    $out = Array();    
    $i = 0;
    foreach ($res as $value) {
      $findme   = '=';
      $pos = strpos($value, $findme);
      //echo $value."<br />";  
        

      if ($pos !== false) {

       $out[$i]['key']= trim(substr($value, 0, $pos));
       $out[$i]['value'] = trim(substr($value, $pos+1));
       $i++;

      }
    }
    return $out;
  }


/*

function originals_file($id){
$this->load->helper('file');
//$string = read_file('./path/to/file.php');

}
*/


	function _originals($id_project,$description)
  {
	  $project = new Project();
		$project->get_by_id($id_project);

    $originals = $this->_parse($description);
    $log ="";
    foreach ($originals as $orig) {
      
      $item = new Item();
      $orig_key= $orig['key'];      
      $item->where('key_string', $orig_key ) ;
	    $item->get();
  		if( ! $item->exists()){
        $item->key_string = $orig['key'];
        $log .= "CREATED ";
      }else {
        $log .= "UPDTED ";
      }
        $item->original_string = $orig['value'];
        $item->save($project);   
        $log .= "[".$item->key_string."]  [".$item->original_string."]<br />" ; 
      }

      return $log;
    }




	function _translations($id, $lang_id, $description)
  {
	  $project = new Project();
		$project->get_by_id($id);

    $translations = $this->_parse($description);
    $log ="";
    $language = new Language();
    $language->get_by_id($lang_id);
    
     

    foreach ($translations as $trans) {     

      $item = new Item();
      $trans_key= $trans['key'];      
      $item->where('key_string', $trans_key ) ;
	    $item->get();
  		if( ! $item->exists()){
        //$item->key_string = $orig['key'];

        $log .= "[NOTSAVED MISSING   ]";
        $log .= " ".$item->key_string." <br />" ;
      }else {
        
        $translation = new Translation();

        $translation->where_related('item', 'id', $item);
        $translation->where_related('language', 'id', $language);
        $translation->get();
  
        $translation->translation_string =$trans['value'];
        $translation->save(
          array(
            'item' => $item,
            'language' => $language
          ));
  
        $log .= "[UPDATED Translation]";
      
        $log .= " ".$item->key_string."  =  ".$translation->translation_string."<br />" ; 
      }
    }

      return $log;
    
  }






// iport original items to project

	function originals($id)
  {
  	

		if($id == 'save')
		{
     	$log = $this->_originals(
			$this->input->post('id'),
      $this->input->post('description'));

      $project = new Project();
      $project->get_by_id($this->input->post('id'));
      $back_link  =  site_url('projects/view/'.$project->id);

        $this->load->view('include/header');
        $this->load->view('include/menubar');
    		$this->load->view('import/originals_result', array('log' => $log, 'back_link' => $back_link, 'project' => $project));
        $this->load->view('include/footer');
			//$save = TRUE;
		}
		else
		{
      $project = new Project();
      $project->get_by_id($id);
		
		  if($project->exists())
		  {
       
        $project->load_extension('htmlform');
		
		    $form_fields = array(
        'id',
			  'description' => array(  
				  'rows' => 26, 
				  'cols' => 40,
          'value'=> "", 
          'label' => 'Import Strings', 
          'class'=> 'form-control'
			    )			
		    );
		
        $url ='import/originals/save';

        $back_link  =  site_url('projects/view/'.$project->id);
        
        $this->load->view('include/header');
        $this->load->view('include/menubar');
    		$this->load->view('import/originals_form', array('project' => $project, 'form_fields' => $form_fields, 'url' => $url, 'back_link' => $back_link));
        $this->load->view('include/footer');

  		}
		  else
		  {
			  show_error('Invalid Project ID');
		  }
    }
  }


	function translations(  $id , $language_id, $action= "" )
  {


		if($action == 'save')
		{
     	$log = $this->_translations(
			$this->input->post('id'),
			$this->input->post('language'),
      $this->input->post('description'));

            $project = new Project();
      $project->get_by_id($this->input->post('id'));
      $back_link  =  site_url('projects/view/'.$project->id);
      
        $this->load->view('include/header');
        $this->load->view('include/menubar');
    		$this->load->view('import/translations_result', array('log' => $log , 'back_link' => $back_link, 'project' => $project));
        $this->load->view('include/footer');
			//$save = TRUE;
		}
		else
		{
  	  $project = new Project();
      $project->get_by_id($id);

      $language = new Language();
      $language->get_by_id($language_id);
		
		  if($project->exists())
		  {
        
        $project->load_extension('htmlform');
		
		    $form_fields = array(
        'id',
        'language' => array(
          'type' => 'hidden',
          'value' =>  $language->id       
          ),
 			  'description' => array(  
				  'rows' => 26, 
				  'cols' => 40,
          'value'=> "", 
          'label' => 'Import Strings', 
          'class'=> 'form-control'
			    )			
		    );
		
        $url ='import/translations/'.$project->id.'/'.$language_id.'/save';

        $back_link  =  site_url('projects/view/'.$project->id);

        $this->load->view('include/header');
        $this->load->view('include/menubar');
    		$this->load->view('import/translations_form', array('project' => $project, 'form_fields' => $form_fields, 'url' => $url, 'back_link' => $back_link));
        $this->load->view('include/footer');

  		}
		  else
		  {
			  show_error('Invalid Project ID');
		  }
    }
  }


}
