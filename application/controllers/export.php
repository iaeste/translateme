<?php

class Export extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('login_manager');
    $this->load->library('pagination');
    $this->load->helper('download');

	}


  function index()
  {
    redirect('/projects/overview/', 'refresh');
  }


  function _originals($id)
  {
    $project = new Project();
  	$project->get_by_id($id);
		if( ! $project->exists())
		{
			show_error('Invalid ID');
		}

   $project->items->order_by("key_string", "asc");

   $items = $project->items->get_iterated();    

    $export_string = "";
    foreach ($items as $item) {
      $export_string .= $item->key_string." = ".$item->original_string."\n" ;
    }
    return $export_string;
  }


  function originals($id)
  {

  $GLOBAL_MESSAGE_ORIGINAL = "messages.properties";
  $GLOBAL_MESSAGE_TRANSLATION_PRE = "messages_";
  $GLOBAL_MESSAGE_TRANSLATION_BACK = ".properties";

    $name = $GLOBAL_MESSAGE_ORIGINAL;
    force_download($name, $this->_originals($id)); 

  }


	function _translations($id, $lang_id)
  {

    $project = new Project();
  	$project->get_by_id($id);
		if( ! $project->exists())
		{
			show_error('Invalid ID');
		}

   $items = $project->items->order_by("key_string", "asc");
   $items->include_related('translation', 'translation_string');   
   $items->where_related('translation/language','id',$lang_id);
   $items = $items->get_iterated();    

    $export_string = "";
    foreach ($items as $item) {
      $export_string .= $item->key_string." = ".$item->translation_translation_string."\n" ;
      //echo $item->key_string." = ".$item->translation_translation_string.'<br />' ;
    }
    return $export_string;

  }

	function translations($id,$language_id)
  {

  $GLOBAL_MESSAGE_ORIGINAL = "messages.properties";
  $GLOBAL_MESSAGE_TRANSLATION_PRE = "messages_";
  $GLOBAL_MESSAGE_TRANSLATION_BACK = ".properties";

    $language = new Language();
 	  $language->get_by_id($language_id);
    $code = $language->code;

    $name = $GLOBAL_MESSAGE_TRANSLATION_PRE.$code.$GLOBAL_MESSAGE_TRANSLATION_BACK;

    //$this->_translations($id, $language_id);
    force_download($name, $this->_translations($id, $language_id)); 
  }

}
