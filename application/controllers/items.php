<?php
class Items extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->model('item');
 
        // load url helper
        $this->load->helper('url');
 
        // load session library
        $this->load->library('pagination');     

		    $this->load->library('login_manager');   
    }
 





function view($id)
{

  $item = new Item();
	$item->get_by_id($id);

	if( ! $item->exists()){
			show_error('Invalid ID');
	}
  else {
    $back_link  =  site_url('items/overview/'.$item->project->id);
    $item->translations->get_iterated();
		$this->load->helper('typography');
    $this->load->view('include/header');
    $this->load->view('include/menubar');
		$this->load->view('items/view', array('item' => $item, 'back_link' => $back_link  ));
    $this->load->view('include/footer');
    }
}



function delete($id)
{
  $item = new Item();
	$item->get_by_id($id);

	if( ! $item->exists()){
			show_error('Invalid ID');
	}
  else {
    $id = $item->project->id;
    $item->delete();
    redirect('/items/overview/'.$id, 'refresh');
  }
} 




function index()
{
  redirect('/projects/overview/', 'refresh');
}


function overview($id, $page_size = 10, $page = 1)
{         
 
  $project = new Project();
	$project->get_by_id($id);
		if( ! $project->exists())
		{
			show_error('Invalid ID');
		}


    $project->items->order_by("key_string", "asc");
    //$project->items->get_iterated();


    $items = $project->items->get_paged($page, $page_size);


   
    $config['uri_segment'] = 5;
    $config['base_url'] =  site_url('items/overview/'.$id.'/'.$page_size.'/');
    $config['total_rows'] = $items->paged->total_rows;
    $config['per_page'] = $page_size;


    $languages = new Language();
    $languages->get_iterated();

    $back_link  =  site_url('projects/view/'.$project->id);


    $this->pagination->initialize($config);

    $this->load->view('include/header');
    $this->load->view('include/menubar');
		$this->load->view('items/list', array('items' => $items, 'languages' => $languages, 'project' => $project , 'back_link' => $back_link ));
    $this->load->view('include/footer');
    $this->load->view('include/dialog_delete');

 
   }



	function edit($id)
	{
		$item = new Item();
		if($id == 'save')
		{
			$item->get_by_id($this->input->post('id'));
			$save = TRUE;
		}
		else
		{
			$item->get_by_id($id);
			$save = FALSE;
		}
		if($item->exists())
		{
			$this->_edit('Edit a Item', 'edit', $item, 'items/edit/save', $save);
		}
		else
		{
			show_error('Invalid Item ID');
		}
	}

	function _edit($title, $section, $item, $url, $save)
	{	

		if($save)
		{

			$item->trans_start();

			$rel = $item->from_array($_POST, array(
        'id',				
        'key_string',
				'original_string',
        'project_id',
        'note'
			
			));

			if($item->save())
			{

				$item->trans_complete();				
				redirect('items/view/' . $item->id);
			}
		}
		

		$item->load_extension('htmlform');

		$form_fields = array(
			'id', 
			'project_id' => array(
        'type'=> 'hidden'
       ), 
			'key_string' => array(
        'class'=> 'form-control'
       ), 
			'original_string' => array(
        'class'=> 'form-control'
       ), 
			'note' => array( 
				'rows' => 6, 
				'cols' => 40,
        'class'=> 'form-control'
			)
			
		);
		
    $this->load->view('include/header');
    $this->load->view('include/menubar');
		$this->load->view('items/edit', array('item' => $item,'title' => $title,  'form_fields' => $form_fields, 'url' => $url));
    $this->load->view('include/footer');
	}










}
?>
