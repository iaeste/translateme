<?php

class Languages extends CI_Controller {
 
    public function __construct()
    {
        parent::__construct();
        $this->load->model('language');
 		$this->load->library('login_manager');
        // load url helper
        $this->load->helper('url');
        $this->load->library('pagination');
 
    }


function index(){


  redirect('/languages/view/', 'refresh');
  
}


	function view($page_size = 10, $page = 1)
	{


    $languages = new Language();
    $languages ->order_by('id', 'ASC');
    $languages->get_paged($page, $page_size);


    $config['base_url'] =  site_url('languages/view/'.$page_size.'/');
    $config['total_rows'] = $languages->paged->total_rows;
    $config['per_page'] = $page_size;




    $this->pagination->initialize($config);




    $this->load->view('include/header');
    $this->load->view('include/menubar');
		$this->load->view('languages/list', array('languages' => $languages));
    $this->load->view('include/footer');
	}

}
