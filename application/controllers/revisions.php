<?php

class Revisions extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('login_manager');
    $this->load->library('pagination');
    $this->load->helper('form');
    $this->load->helper('url');
    $this->load->helper('base64_url');
	}
	

function overview ($project_id=0, $lang_id=0, $page_size = 10, $page = 1)
{

    if($project_id==0){  
      return; 
    }
    if($lang_id==0){  
      return; 
    }
      
    $lang = new Language();
    $lang->get_by_id($lang_id);
    if( ! $lang->exists()){
      return; 
    }


    $project = new Project();
    $project->get_by_id($project_id);
    if( ! $project->exists()){
     return; 
    }

    $back_link  =  site_url('projects/view/'.$project->id);

    $items =  $project->items;

    $items->order_by("key_string", "asc");

//    $items->where_related('project/id','id',$project_id);
    // $items->include_related('translation', 'translation_string')
     
   // $items->where_related('translation/language','id',$lang_id);

  //  $items->get(); 
    $items->get_paged($page, $page_size);

    $config['uri_segment'] = 6;
    $config['base_url'] =  site_url('translations/overview/'.$project_id.'/'.$lang_id.'/'.$page_size.'/');
    $config['total_rows'] = $items->paged->total_rows;
    $config['per_page'] = $page_size;
    $this->pagination->initialize($config);

    $back_url_encoded= base64_url_encode(uri_string());
    

     $this->load->view('include/header');
     $this->load->view('include/menubar');

  	  $this->load->view('revisions/list', array('project' => $project, 'items' => $items, 'lang' => $lang, 'back_link' => $back_link,  'back_url_encoded' => $back_url_encoded));

           $this->load->view('include/footer');


}


function start($project_id=0, $lang_id=0){
    if($project_id==0){  
      return; 
    }
    if($lang_id==0){  
      return; 
    }


    $project = new Project();
    $project->get_by_id($project_id);
    if( ! $project->exists()){
     return; 
    }


   
    
    $back_url_encoded= base64_url_encode('projects/view/'.$project->id);

    $item =  $project->items;
    $item->order_by("key_string", "asc");
    $item ->limit(1); 
    $item->get();

    $item->get();
	  $this->make($item->id, $lang_id, $back_url_encoded, $save = false);
}

 


function index(){


  redirect('/projects/overview/', 'refresh');
  
}

function _save()
{
  $translation = new Translation();
  $id = $this->input->post('id');
  $translation->get_by_id($id);
  //$translation->where('id', $id)->get();

//echo $id;
 //echo $this->input->post('language');
 // echo $this->input->post('item');
 // echo $this->input->post('translation_string');

  
			if( ! $translation->exists())
			{
//echo 'Non existing' ;
}

$translation->translation_string = $this->input->post('translation_string');


  $language_id =$this->input->post('language');
  $language = new Language();
  $language->get_by_id($language_id);

//echo $language->id;
  $item_id =$this->input->post('item');
  $item = new Item();
  $item->get_by_id($item_id);
  



//$translation->language = $language;
//$translation->item = $item;


  $translation->save(
    array(
        'item' => $item,
        'language' => $language
    ));




/*$rel = $translation->from_array($_POST, array(
 		'id',
    'language',
    'item',
    'translation_string'
			),TRUE);

  */

 //   echo $translation->language;
  //  echo $translation->item;
	//	echo $translation = $translation->exists();
	//	$translation->save($rel);



   }

	function make($item_id, $lang_id, $encoded_back_url, $save = false)
	{
		if($save){
      $this->_save($item_id);
      $action = $this->input->post('action');
      if ($action!==""){
              redirect($action);
      }

    }
    

		$this->_make_translation($item_id, $lang_id, $encoded_back_url);
		
	}



	function _make_translation($item_id, $lang_id, $encoded_back_url )
	{

//    $translation->translation ="Super trpouper";

    $back_url = site_url(base64_url_decode($encoded_back_url));
    $item = new Item();
	  $item->get_by_id($item_id);

    $language = new Language();
	  $language->get_by_id($lang_id);

    $translation = new Translation();

    $translation->where_related('item', 'id', $item);
    $translation->where_related('language', 'id', $language);
    $translation->get();
    
if( ! $translation->exists())
			{
        $translation->item=$item;
        $translation->language=$language;
        //Maybee here something like this
	      //$project->include_related('creator', 'name', TRUE, TRUE);
		    //$project->include_related('editor', 'name', TRUE, TRUE);
				// ...and creator for new projects
				//$rel['creator'] = $this->login_manager->get_user();
			}

		
   


    $item_next = new Item();
    $item_next->where('id >', $item_id);
    $item_next->select_min('id');
    $item_next->limit(1);
    $item_next->get(); 

    $url_item_next_class="";
     
    if ($item_next->id){
      $url_item_next =  site_url('translations/make/'.$item_next->id.'/'.$lang_id.'/'.$encoded_back_url);
    } else {
      $url_item_next =  site_url('translations/make/'.$item->id.'/'.$lang_id.'/'.$encoded_back_url);
      $url_item_next_class="disabled";
    }

    $item_back = new item();
    $item_back->where('id <', $item_id);
    $item_back->select_max('id');
    $item_back->limit(1);
     $item_back->get(); 


      $url_item_back_class="";
    if ($item_back->id){
      $url_item_back =  site_url('translations/make/'.$item_back->id.'/'.$lang_id.'/'.$encoded_back_url);
    }
    else {
      //echo 'not exist';
      $url_item_back =  site_url('translations/make/'.$item->id.'/'.$lang_id.'/'.$encoded_back_url);
      $url_item_back_class="disabled";
    }      

    $url = site_url('translations/make/'.$item->id.'/'.$lang_id.'/'.$encoded_back_url.'/'.'save');
      

    $this->load->view('include/header');
    $this->load->view('include/menubar');

		$this->load->view('translations/make', array('url_item_next' =>$url_item_next, 'url_item_back' => $url_item_back, 'translation' => $translation, 'url' => $url, 'back_url' => $back_url, 'url_item_next_class' => $url_item_next_class, 'url_item_back_class' => $url_item_back_class ));

           $this->load->view('include/footer');

   }




}
