<?php

class Projects extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('login_manager');
    $this->load->library('pagination');
	}
	

  function index()
  {
    redirect('/projects/overview/', 'refresh');
  }


	function overview($page_size = 10, $page = 1)
	{

    $projects = new Project();

    $projects->get_paged($page, $page_size);
    $config['base_url'] =  site_url('projects/overview/'.$page_size.'/');
    $config['total_rows'] = $projects->paged->total_rows;
    $config['per_page'] = $page_size;
    $this->pagination->initialize($config);

    $this->load->view('include/header');
    $this->load->view('include/menubar', array('title' => 'Projects', 'section' => 'projects'));
		$this->load->view('projects/list', array('projects' => $projects));
    $this->load->view('include/footer');
    $this->load->view('include/dialog_delete');

   }



/**
** View of current project 
**/
	function view($id)
	{
		$project = new Project();

		$project->include_related('creator', 'name', TRUE, TRUE);
		$project->include_related('editor', 'name', TRUE, TRUE);
		$project->get_by_id($id);

		if( ! $project->exists())
		{
			show_error('Invalid Project ID');
		}
	

    $back_link  =  site_url('projects/overview/');
  
    $languages = new Language();
    $languages->get_iterated();


    $items = new Item();
    $total_count= $project->items->count();

		
		$this->load->helper('typography');			
   
    $this->load->view('include/header');
    $this->load->view('include/menubar');
		$this->load->view('projects/view', array('project' => $project, 'languages' => $languages, 'back_link' => $back_link, 'total_count'=>$total_count));
    $this->load->view('include/footer');
	}


  function delete($id)
	{
		$project = new Project();
		$project->get_by_id($id);

		if($project->exists())
		{
			$project->delete();
      redirect('/projects/overview/', 'refresh');
		}
		else
		{
			show_error('Invalid Project ID');
		}

  }

	function create($save = FALSE)
	{
    $project = new Project();
    $this->_edit('Create Project', 'create', $project, 'projects/create/save', $save);
	}


	function edit($id)
	{
		$project = new Project();
		if($id == 'save')
		{
			$project->get_by_id($this->input->post('id'));
			$save = TRUE;
		}
		else
		{
			$project->get_by_id($id);
			$save = FALSE;
		}
		if($project->exists())
		{
			$this->_edit('Edit a Project', 'edit', $project, 'projects/edit/save', $save);
		}
		else
		{
			show_error('Invalid Project ID');
		}
	}

/**
	 * Called by the edit and report segments. 
	 * 
	 * @param string $title For the header
	 * @param string $section For the header
	 * @param Project $project Project to edit or a blank project
	 * @param string $url The url to save on
	 * @param boolean $save If TRUE, then attempt a save.
	 */
	function _edit($title, $section, $project, $url, $save)
	{	

		if($save)
		{

			$project->trans_start();

			$rel = $project->from_array($_POST, array(
        'id',				
        'title',
				'description'
			
			));

			$rel['editor'] = $this->login_manager->get_user();
			if( ! $project->exists())
			{
				$rel['creator'] = $this->login_manager->get_user();
			}

			$exists = $project->exists();

			if($project->save())
			{

				$project->trans_complete();				

        if($exists)
				{
					$this->session->set_flashdata('message', 'This project was updated successfully.');
				}
				else
				{
					$this->session->set_flashdata('message', 'This project was created successfully.');
				}
				redirect('projects/view/' . $project->id);
			}
		}
		

		$project->load_extension('htmlform');

		$form_fields = array(
			'id', 
			'title' => array(
        'class'=> 'form-control'
       ), 
			'description' => array( 
				'rows' => 6, 
				'cols' => 40,
        'class'=> 'form-control'
			)
			
		);
		
    $this->load->view('include/header');
    $this->load->view('include/menubar');
		$this->load->view('projects/edit', array('project' => $project,'title' => $title,  'form_fields' => $form_fields, 'url' => $url));
    $this->load->view('include/footer');
	}

}
