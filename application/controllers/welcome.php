<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {


	function __construct()
	{
		parent::__construct();
		$this->load->library('login_manager');
		$this->load->model('project');
	}
	

	function index()
	{
	  redirect('/projects/overview/');


	}
}


