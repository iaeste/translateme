<?php
/**
 * Project DataMapper Model
 *
 * The core class for the application
 *
 * @license	MIT License
 * @category	Models
 * @author  	David Cerny
 * @link    	http://www.senman.cz
 */
class Project extends DataMapper {
 
	// --------------------------------------------------------------------
	// Relationships
	// --------------------------------------------------------------------

	// Insert related models that Bug can have just one of.
  public $has_one = array(
		
		'creator' => array(
			'class' => 'user',
	 		'other_field' => 'created_project'
	 	),

		// The creator of project
		'editor' => array(
			'class' => 'user',
	 		'other_field' => 'created_project'
	 	)

	 );
	
   public $has_many = array(

		'item'

   );

	// --------------------------------------------------------------------
	// Validation
	// --------------------------------------------------------------------
	
	public $validation = array(
		'title' => array(
			'rules' => array('required', 'trim', 'max_length' => 255)
		),
		'description' => array(
			'rules' => array('required', 'xss_clean'),
			'type' => 'textarea'
		),

		'creator' => array(
			'rules' => array('required')
		),

		'editor' => array(
			'rules' => array('required')
		),
	);

}
?>
