<?php
/**
 * Translation DataMapper Model
 *
 * The core class for the application
 *
 * @license	MIT License
 * @category	Models
 * @author  	David Cerny
 * @link    	http://www.senman.cz
 */

class Translation extends DataMapper {
 
	// --------------------------------------------------------------------
	// Relationships
	// --------------------------------------------------------------------
	

    public $has_one = array(

	// The translation of this item
	'item' => array (
		'class' => 'item'
),



	'language' => array (
		'class' => 'language'
),



	'state' => array (
		'class' => 'state'
)



	);

 


	// --------------------------------------------------------------------
	// Validation
	// --------------------------------------------------------------------
	
	public $validation = array(
		'translation_string' => array(
			'rules' => array('required'),
			'type' => 'textarea'
		),
	//what is xss clean
	//	'note' => array(
	//		'rules' => array('required', 'xss_clean'),
//			'type' => 'textarea'
//		),

	//	'approved' => array(
	//		'rules' => array('required')
	//	),
	//	'edited' => array(
	//		'rules' => array('required')
//		),
	//	'status' => array(
	//		'rules' => array('required')
	//	)
	);





}
?>
