<?php
/**
 * Item DataMapper Model
 *
 * The core class for the application
 *
 * @license	MIT License
 * @category	Models
 * @author  	David Cerny
 * @link    	http://www.senman.cz
 */
class Item extends DataMapper {
 
	// --------------------------------------------------------------------
	// Relationships
	// --------------------------------------------------------------------

	// Insert related models that Bug can have just one of.
	public $has_one = array(
		
		'project' => array(
			'class' => 'project'

	 	//	'other_field' => 'projects_item'
	 	)

	 );

	
    	public $has_many = array(

		'translation'=> array(
			'class' => 'translation'
	 	)

	);
 



}
?>
