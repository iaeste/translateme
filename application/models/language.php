<?php
/**
 * Language DataMapper Model
 *
 * The core class for the application
 *
 * @license	MIT License
 * @category	Models
 * @author  	David Cerny
 * @link    	http://www.senman.cz
 */
class Language extends DataMapper {


  	// --------------------------------------------------------------------
	// Relationships
	// --------------------------------------------------------------------

	public $has_many = array('translation');
 	// --------------------------------------------------------------------
	// Validation
	// --------------------------------------------------------------------
	
	public $validation = array(
		'name' => array(
			'rules' => array('required', 'trim', 'max_length' => 100)
		),
		'code' => array(
			'rules' => array('required', 'trim', 'max_length' => 100)
		)
	);



}
?>
