CREATE TABLE IF NOT EXISTS `translations` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `translation_string` text NOT NULL,
  `item_id` bigint(20) NOT NULL,
  `language_id` bigint(20) NOT NULL,
  `state_id` bigint(20) NOT NULL,
  
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  ;

