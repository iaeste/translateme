CREATE TABLE IF NOT EXISTS `items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_string` varchar(255) CHARACTER SET utf8 NOT NULL,
  `original_string` text CHARACTER SET utf8 NOT NULL,
  `project_id` bigint(20) NOT NULL,
  `note` TEXT CHARACTER SET utf8 NOT NULL, 
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;

