<html>
    <head>
        <title>CodeIgniter Sample Application</title>
        <link rel="stylesheet" type="text/css" media="screen" href="<?= base_url() ?>css/style.css">
    </head>
<body>
 
<div id="container">
 
<h1>Datamapper Tutorial</h1>
 

<table>
	<thead>
	<tr>
		<th>Item</th>
		<th>Translation</th>
	<th>Language</th>
	</tr>	
	</thead>
	<tbody>

	<?php foreach($item_list as $item): ?>	
	<?php $item->translation->get()->order_by('language_id'); ?>


 	<tr>
		<td><?= $item->key_string ?></td>
		<td>
		    <? foreach($item->translation->all as $translation): ?>
		    <?= $translation->translation_string ?><br/>
<?= $item->translation->language->get()->name  ?>
		    <? endforeach ?>
		</td>
	
	
	<?php endforeach ?>
	</tbody>
</table>
 
 
<p class="pagination"><?= $this->pagination->create_links(); ?></p>
 
</div>
 
</body>
</html>
