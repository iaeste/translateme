
<a href="<?=$back_link?>" class="btn btn-primary btn-lg">Back</a>

<h1>Original Items: <?=$project->title ?>  </h1>



<table class="table table-bordered">

	<tr>

		<th class="code" width="30%">Key Code</th>
		<th class="original" width="40%">Original</th>
		<th class="translations" width="20%" >Note</th>
		<th class="action" width="100">Action</th>

	</tr>
<?php	if($items->result_count() < 1): ?>
	<tr>
		<td colspan="3">No items Found.</td>
	</tr>
<?php	else: ?>
<?php		$odd = FALSE;
		foreach($items as $item):
			$odd = !$odd;
		?>
	<tr class="<?php echo $odd ? 'odd' : 'even'; ?>">

		<td class="key_string"><code><?php echo htmlspecialchars($item->key_string); ?></code></td>
		<td class="original_string">

<?php 
$out = $item->original_string;
if(strlen($out) > 40)
  $out = substr($out, 0, 40).'...';

echo htmlspecialchars($out); ?>



</td>

<td>
</td>


<td class="action">

<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li>
      <a href="<?php echo site_url('items/view/' . $item->id); ?>" title="View">View</a>
    </li> 
    <li>
			<a href="<?php echo site_url('items/edit/' . $item->id); ?>" title="Edit">Edit</a>
    </li>

    <li>
			<a  class="confirm-delete" href="<?php echo site_url('items/delete/' . $item->id); ?>" title="Delete">Delete</a>
    </li>


  </ul>
</div>

</td>



	</tr>
<?php	endforeach; ?>
<?php	endif; ?>
</table>

	
	
<?= $this->pagination->create_links(); ?>




