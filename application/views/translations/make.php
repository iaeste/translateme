<a class="btn btn-primary btn-lg" href="<?=$back_url?>"> Back</a>

<h1>Make Translation </h1>

<form role="form" action="<?=$url ?>" method="post">

<div class="row">
  <div class="col-md-6">
    <h2>   Original </h2>
  </div>
  <div class="col-md-6">
    <h2 class="pull-right"><?= $translation->language->name ?> </h2>
  </div>
</div>

<div class="row" >
  <div class="col-md-6" >
    <div class="panel panel-default" style="height: 250px;">
      <div class="panel-heading">
        <h2 class="panel-title "><?=$translation->item->key_string ?></h2>
      </div>
      <div class="panel-body">
        <?php echo htmlspecialchars($translation->item->original_string); ?>
      </div>
    </div>
  </div>
  
  <div class="col-md-6" >
    <div class="panel panel-default" style="height: 250px;">
      <div class="panel-heading">
        <h2 class="panel-title ">Translation</h2>
      </div>
      <div class="panel-body">


  <input class="integer" type="hidden" name="id" id="id" value="<?=$translation->id ?>" />
  <input  type="hidden" name="language" id="language" value="<?=$translation->language->id ?>" />
  <input  type="hidden" name="item" id="item" value="<?=$translation->item->id ?>" />  


  <input  type="hidden" name="url_item_next"  value="<?=$url_item_next ?>" />  
  <input  type="hidden" name="url_item_back"  value="<?=$url_item_back ?>" />  


  <div class="form-group"  id="row_translation_string">
    <textarea rows="6" cols="40" class="form-control required" name="translation_string" id="translation_string"><?=$translation->translation_string ?></textarea>			 
  </div>


      </div>
    </div>
  </div>
</div>

<div class="row">

  <div class="col-md-12" >

    <div class ="pull-right"> 
      <a class="btn btn-primary btn-lg <?=$url_item_back_class?>" href="<?=$url_item_back?>">Previous</a>
       <button class="btn btn-primary btn-lg <?=$url_item_back_class?>" type="submit" name="action" value="<?=$url_item_back?>" >Save and Previous </button>
      <button class="btn btn-primary btn-lg" type="submit" name="action" value="" > Save</button>
      <button class="btn btn-primary btn-lg <?=$url_item_next_class?>" type="submit" name="action" value="<?=$url_item_next?>" >Save and Next</button>

      <a class="btn btn-primary btn-lg <?=$url_item_next_class?>" href="<?=$url_item_next?>">Next</a>
    </div>
  </div>  
</div>

</form>

