<a href="<?=$back_link?>" class="btn btn-primary btn-lg">Back</a>

<div class="row">

 <div class="col-md-6">

<h1>Translations: <?=$project->title ?> </h1>


</div>

  <div class="col-md-6">
<h1 class="pull-right">
<?= $lang->name ?>
</h1>


</div>

</div>


<p>



</p>



<table class="table table-bordered">

	<tr>

		<th class="code" width="20%" >Code</th>
		<th class="original" width="35%" >Original</th>
		<th class="translation" width="35%">Translations</th>
		<th class="action" width="100">Action</th>

	</tr>
<?php	if($items->result_count() < 1): ?>
	<tr>
		<td colspan="3">No items Found.</td>
	</tr>
<?php	else: ?>
<?php		$odd = FALSE;
		foreach($items as $item):
			$odd = !$odd;
		?>
	<tr class="<?php echo $odd ? 'odd' : 'even'; ?>">


		<td class="key_string"><code><?php echo htmlspecialchars($item->key_string); ?></code></td>
		<td class="original_string">

<?php 
$out = $item->original_string;
if(strlen($out) > 40)
  $out = substr($out, 0, 40).'...';

echo htmlspecialchars($out); ?>



</td>
<td class="translations">  
<?= 

$item->translation->where_related('language','id',$lang->id)->get()->translation_string;
echo $item->translation_translation_string; 

?>

</td>
<?php 


    $url = site_url('translations/make/'.$item->id.'/'.$lang->id.'/'.$back_url_encoded ); 
    
?>

<td class="action">
			<a href="<?=$url?>" class="btn btn-success" title="Translate">Translate</a>


</td>



	</tr>
<?php	endforeach; ?>
<?php	endif; ?>
</table>



	
	
<?= $this->pagination->create_links(); ?>




