<div class="searchSection box">
<h3>Search</h3>
<?php

	echo $projects->render_form($form_fields, $url, array('save_button' => 'Search', 'reset_button' => TRUE));

?>
</div>

<div class="searchResults">
<?php

if( ! empty($projects))
{

	$paging = $this->load->view('projects/paging', array('projects' => $projects), TRUE);

	echo($paging);

	$this->load->view('projects/list', array('projects' => $projects));

	echo($paging);
}
?>

</div>

<span class="clear"></span>
