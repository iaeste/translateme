

<div class="row">
<h1>Projects </h1>

<p>Projects awaiting for translation.
</p>

</div>

<div class="row">


<table class="table table-bordered">

	<tr>

		<th class="title" width="20%">Title</th>
		<th class="status" width="70%">Description</th>
		<th class="buttons" width="100">Actions</th>
	</tr>
<?php	if($projects->result_count() < 1): ?>
	<tr>
		<td colspan="4">No Projects Found.</td>
	</tr>
<?php	else: ?>
<?php		$odd = FALSE;
		foreach($projects as $project):
			$odd = !$odd;
		?>
	<tr class="<?php echo $odd ? 'odd' : 'even'; ?>">

		<td class="title"><b><a href="<?php echo site_url('projects/view/' . $project->id); ?>" title="View this Project"> <?php echo htmlspecialchars($project->title); ?></a></b></td>
		<td class="status"><?php echo htmlspecialchars($project->description); ?></td>
		<td class="buttons">



<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li>
			<a href="<?php echo site_url('projects/view/' . $project->id); ?>" title="View">View</a>
</li>

    <li>

			<a href="<?php echo site_url('projects/edit/' . $project->id); ?>" title="Edit">Edit</a>
</li>

    <li>
<a class="confirm-delete" href="<?php echo site_url('projects/delete/' . $project->id); ?>" title="Delete">Delete</a>

</li>
  </ul>
</div>




			&nbsp;





		</td>
	</tr>
<?php	endforeach; ?>
<?php	endif; ?>
</table>


	
<?= $this->pagination->create_links(); ?>



</div>







