
<a href="<?=$back_link?>" class="btn btn-primary btn-lg">Back</a>

<h1>Project: <?=$project->title?></h1>


<div class="row">
  <div class="col-md-10">

    <?php echo auto_typography($project->description); ?>


<dl class="dl-horizontal">
  <dt>Created By:</dt>
<dd> <?php echo htmlspecialchars($project->creator->name); ?></dd>


	<dt>Date Created:</dt>
<dd> <?php echo strftime('%A, %B %e, %G at %l:%M %P', strtotime($project->created)); ?></dd>


  <dt>Last Edited By:</dt><dd> <?php echo htmlspecialchars($project->editor->name); ?></dd>


  <dt>Last Updated:</dt><dd> <?php echo strftime('%A, %B %e, %G at %l:%M %P', strtotime($project->updated)); ?></dd>


</dl>

  </div>

  <div class="col-md-2">
<div class="btn-group-vertical">


<a class="btn btn-default" href="<?php echo site_url('import/originals/' . $project->id); ?>" title="Edit this Project">Import Original Strings</a>

<a class="btn btn-default" href="<?php echo site_url('export/originals/' . $project->id); ?>" title="Edit this Project">Export Original Strings</a>

<a class="btn btn-default" href="<?php echo site_url('items/overview/' . $project->id); ?>" title="Edit this Project">Show Original Strings</a>


<a class="btn btn-default disabled" href="<?php echo site_url('export/originals/' . $project->id); ?>" title="Edit this Project">Export All Strings</a>
</div>
</div>
</div>





<div class="row">
<h2>Translation Status</h2> 




<table class="table table-bordered">

	<tr>
	
		<th class="lang" width="20%">Language</th>
		<th class="stat" width="20%">Translation Status</th>

		<th class="stat" width="25%" colspan=2 >Progress</th>
	
    <th class="stat" width="25%" colspan=2 >To Revise</th>

		<th class="action" width="100">Action</th>

	</tr>
<?php	if($languages->result_count() < 1): ?>
	<tr>
		<td colspan="3">No items Found.</td>
	</tr>
<?php	else: ?>
<?php		$odd = FALSE;
		foreach($languages as $lang):
			$odd = !$odd;
		?>
	<tr class="<?php echo $odd ? 'odd' : 'even'; ?>">



<?



    $items = new Item();
    $items->where('project_id', $project->id);
    $translation_count= $items->where_related('translation/language','id',$lang->id)->count();
    $percentage = ceil($translation_count / $total_count * 100);

    $items = new Item();
    $items->where('project_id', $project->id);
    $translation_state_count= $items->where_related('translation/language','id',$lang->id)->where_related('translation/state','>id',0) ->count();



?>


		<td class="key_string"> 

<a  href="<?= site_url('translations/overview/'. $project->id .'/'. $lang->id); ?>" title="View"> 
 <b><?php echo htmlspecialchars($lang->name); ?></b>
</a>

</td>




<td>

<div class="progress">
  <div class="progress-bar " style="width: <?=$percentage?>%">
    <span class="sr-only"><?=$percentage?>% completed</span>
  </div>
</div>

</td>

<td  width="100" >
  <?=$percentage?>% completed
</td>

<td>
  <a href="<?= site_url('translations/start/'. $project->id .'/'. $lang->id); ?>" class="btn btn-success ">Start Translating </a>  
</td>


<td  width="100" >
  <?= $translation_state_count?>
</td>

<td>
  <a href="<?= site_url('revisions/start/'. $project->id .'/'. $lang->id); ?>" class="btn btn-success ">Start Revising</a>
</td>


<td>


<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
    Action <span class="caret"></span>
  </button>
  <ul class="dropdown-menu" role="menu">
    <li>
<a  href="
<?= site_url('translations/overview/'. $project->id .'/'. $lang->id); ?>" title="View"> 
Show Translations
</a>
</li>

    <li>

<a  href="<?= site_url('import/translations/'. $project->id .'/'. $lang->id); ?>" title="View"> 
Import Translations</a>
</li>

    <li>
<a  href="<?= site_url('export/translations/'. $project->id .'/'. $lang->id); ?>" title="View"> 
Export Translations</a>

</li>
  </ul>
</div>




</td>

	</tr>
<?php	endforeach; ?>
<?php	endif; ?>
</table>




</div>










<span class="clear"></span>
