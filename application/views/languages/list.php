<h1>Languages </h1>

<p>Languages to translation
</p>
<table class="table table-bordered">

	<tr>
		<th class="id">ID</th>
		<th class="Name">Name</th>
		<th class="code">Code</th>

	</tr>
<?php	if($languages->result_count() < 1): ?>
	<tr>
		<td colspan="3">No languages Found.</td>
	</tr>
<?php	else: ?>
<?php		$odd = FALSE;
		foreach($languages as $language):
			$odd = !$odd;
		?>
	<tr class="<?php echo $odd ? 'odd' : 'even'; ?>">

		<td class="id"><?php echo $language->id; ?></td>
		<td class="name"> <?php echo htmlspecialchars($language->name); ?></td>
		<td class="code"><?php echo htmlspecialchars($language->code); ?></td>
		

	</tr>
<?php	endforeach; ?>
<?php	endif; ?>
</table>



	
<?= $this->pagination->create_links(); ?>



