<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
<div class="container">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="<?php echo site_url('welcome'); ?>">TranslateMe</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    <ul class="nav navbar-nav">




<?php
	$user = isset($this->login_manager) ? $this->login_manager->get_user() : FALSE;

	if( ! isset($message))
	{
		$message = $this->session->flashdata('message');
	}

?>


    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects <b class="caret"></b></a>
        <ul class="dropdown-menu">
         <li><a href="<?php echo site_url('projects'); ?>" type="button">Projet List</a>
         <li><a href="<?php echo site_url('projects/create'); ?>" type="button">Create Project</a></li>

        </ul>


</li>

<li><a href="<?php echo site_url('languages'); ?>" >Languages</a></li>
      

<li><a href="<?php echo site_url('users'); ?>" >Users</a></li>

    </ul>



    <ul class="nav navbar-nav navbar-right">

<?php
if($user == FALSE){ ?> 


      <li><a href="<?php echo site_url('login'); ?>">Nepřihlášený uživatel </a></li>
<?php }else{ ?>

<li><a href="#">	<?php echo htmlspecialchars($user->name); ?> </a> </li>

<li><a href="<?php echo site_url('logout'); ?>">Logout</a>  </li>
      
<?php } ?>
    </ul>
  </div><!-- /.navbar-collapse -->

</div>

</nav>

