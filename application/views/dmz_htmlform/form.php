<?php
	if( ! isset($save_button))
	{
		$save_button = 'Save';
	}
	if( ! isset($reset_button))
	{
		$reset_button = FALSE;
	}
	else
	{
		if($reset_button === TRUE)
		{
			$reset_button = 'Reset';
		}
	}
?>
<?php if( ! empty($object->error->all)): ?>
<div class="error">
	<p>There was an error saving the form.</p>
	<ul><?php foreach($object->error->all as $k => $err): ?>
		<li><?php echo $err; ?></li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>




<form role="form" action="<?php echo $this->config->site_url($url); ?>" method="post">

<?php echo $rows; ?>

		<input class="btn btn-default" type="submit" value="<?php echo $save_button; ?>" /><?php
			if($reset_button !== FALSE)
			{
				?> <input class="btn btn-default" type="reset" value="<?php echo $reset_button; ?>" /><?php
			}
		?>
</form>
