<h1>Login</h1>
		<p>Welcome to the TranslateMe. Tool for localization of Grails i18n files. Please log in.</p>
<?php

	echo $user->render_form(array(
			'username' => array(   
        'label' => 'Username',
        'class'=> 'form-control'),



			'password' => array(   
        'label' => 'Password',
        'class'=> 'form-control')
		),
		'login',
		array(
			'save_button' => 'Log In',
			'reset_button' => 'Clear'
		)
	);

?>
	
