<?php

	if(isset($title))
	{
		$page_title = $title . ' - ';
	}
	else
	{
		$title = '';
		$page_title = '';
	}



?>
<!DOCTYPE html>
<html>
  <head>
    <title>Bootstrap 101 Template</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo str_replace('index.php/', '', site_url('css/bootstrap.min.css')); ?>" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="../../assets/js/html5shiv.js"></script>
      <script src="../../assets/js/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <h1>Hello, world!</h1>

<!--
Squash Bug Tracker is licensed under the Creative Commons Attribution-Share Alike 3.0 United States License
More Info: http://creativecommons.org/licenses/by-sa/3.0/us/
-->
<!-- Header -->
<div class="header">
	<h1 title="Squash Bug Tracker">Squash Bug Tracker</h1>



</div>
<!-- End Header -->

<?php if( ! empty($title)): ?>
<!-- Page Title -->
<h2><?php echo $title; ?></h2>
<?php endif; ?>

<!-- Page Content -->
<div class="content">

<?php if( ! empty($message)): ?>
<!-- Form Result Message -->
<div id="page_message"><?php echo htmlspecialchars($message); ?></div>
<?php endif; ?>
