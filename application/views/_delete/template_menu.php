<?php	if(!isset($section))
	{
		$section = 'welcome';
	}

	$sections = array(
		'welcome' => array(
			'name' => 'Welcome',
			'url' => 'welcome'
		),
		'search' => array(
			'name' => 'Projects',
			'url' => 'projects'
		),
		'report' => array(
			'name' => 'Create Project to Translate',
			'url' => 'projects/create'
		),
		'admin' => array(
			'name' => 'Admin',
			'url' => 'admin',
			'restrict' => 1
		),
		'logout' => array(
			'name' => 'Log Out',
			'url' => 'logout'
		)
	);

	$user = isset($this->login_manager) ? $this->login_manager->get_user() : FALSE;

	if( ! isset($message))
	{
		$message = $this->session->flashdata('message');
	}

 if(!isset($hide_nav) || !$hide_nav): ?>
	<div class="nav">
		<ul>
<?php			foreach($sections as $key => $s):
				if($user !== FALSE)
				{
					if(isset($s['restrict']))
					{
						if($user->group->id > $s['restrict'])
						{
							continue;
						}
					}
				}
				$sel = ($section == $key) ? ' selected' : ''; ?>
			<li class="<?php echo $key . $sel; ?>"><a href="<?php echo site_url($s['url']); ?>"><?php echo $s['name']; ?></a></li>
<?php			endforeach; ?>

		</ul>
	</div>
<?php if($user !== FALSE): ?>
	<div class="username">Welcome, <?php echo htmlspecialchars($user->name); ?></div>
<?php endif; ?>
<?php endif; ?>
